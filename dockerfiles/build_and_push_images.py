#!/usr/bin/env python

""" Script to build and push Docker images for the CI pipelines to the GitLab container registry
    for this project.

    Instructions:
    Before running this script you'll need to create a Personal Access Token (PAT) in your GitLab
    account. See: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

    Then you'll need to set that token to be used by `docker push` for pushing the images:
    `docker login -u <PERSONAL_ACCESS_TOKEN_NAME> -p <PERSONAL_ACCESS_TOKEN_KEY> registry.gitlab.com`

    Then you can run this script:
    python dockerfiles/build_and_push_images.py
"""

from pathlib import Path
import subprocess

PYTHONS = [
    "38",
    "39",
    "310",
    "311",
    "312",
]

DOCKERFILES_DIR = Path(__file__).resolve(strict=True).parent


for version in PYTHONS:
    python = f"python{version}"
    print(f"Building image for {python}...")
    command = [
        "docker",
        "build",
        "--platform",
        "linux/amd64",  # Assuming GitLab CI is using x86 machines, for now
        "-t",
        f"registry.gitlab.com/potato-oss/google-cloud/django-gcloud-connectors/{python}",
        DOCKERFILES_DIR / python,
    ]
    subprocess.check_call(command)
    print(f"Successfully built image for {python}.")

    print(f"Pushing image for {python}...")
    command = [
        "docker",
        "push",
        f"registry.gitlab.com/potato-oss/google-cloud/django-gcloud-connectors/{python}:latest"
    ]
    subprocess.check_call(command)
    print(f"Successfully pushed image for {python}.")

print("Pushed new images for django-gcloud-connectors to GitLab.")
