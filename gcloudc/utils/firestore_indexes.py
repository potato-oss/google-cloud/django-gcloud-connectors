""" A utility for updating the indexes in your Firestore indexes file with any new indexes that have
    been used by the Firestore emulator during its current run.
"""

from __future__ import annotations
from dataclasses import dataclass
from functools import total_ordering
from typing import List, Literal, Optional
import logging
import json
import os
import urllib.error
import urllib.request

logger = logging.getLogger(__name__)


class IndexingError(Exception):
    """ Common exception class to allow catching of expected possible errors from this script
        (distinct from bugs in the script).
    """


def update_firestore_indexes(
    emulator_project,
    emulator_config="firebase.json",
    indexes_file="firestore.indexes.json"
):
    """ Given details of the local Firestore emulator and the local indexes definitions file, update
        that definitions file with any additional indexes which the emulator has recorded as being
        used within its current run.
    """
    emulator_port = get_emulator_port_from_config(emulator_config)
    emulator_host = get_emulator_host_from_config(emulator_config)
    current_indexes = load_indexes_from_definitions_file(indexes_file)
    new_indexes = get_indexes_from_emulator(emulator_port, emulator_project, emulator_host=emulator_host)
    combined_indexes = combine_indexes(current_indexes, new_indexes)
    write_indexes_to_definitions_file(combined_indexes, indexes_file)


def get_value_from_config(value, config_path):
    """Get a configuration value for Firestore from the given Firebase emulator's config path.
    """
    with open(config_path) as file:
        return json.load(file)["emulators"]["firestore"][value]


def get_emulator_port_from_config(config_path):
    """Get the local port number for Firestore.
    """
    return get_value_from_config("port", config_path)


def get_emulator_host_from_config(config_path):
    """Get the local host name for Firestore.
    """
    return get_value_from_config("host", config_path)


def load_indexes_from_definitions_file(indexes_file) -> List[Index]:
    """ If the index definitions file at the given path exists and contains some non-empty JSON,
        return the indexes defined by that JSON.
    """
    if os.path.exists(indexes_file):
        with open(indexes_file) as file:
            data = file.read()
            if data.strip():
                data = json.loads(data)
                if data:
                    return Index.list_from_definitions_file_format(data)
    return []


def get_indexes_from_emulator(emulator_port, emulator_project, emulator_host="127.0.0.1") -> List[Index]:
    """ Get indexes which have been used by the emulator during its current run, optionally filtered
        to the specified project.
    """
    # Note that even though this URL appears to specify the project, it seems that the indexes are
    # only ever supplied on the URL for the project which the emulator was started for (even when
    # it's started in multi-project mode). Also, the emulator doesn't yet (April 2024) support
    # multiple databases per project, so we always use "(default)"
    url = (
        f"http://{emulator_host}:{emulator_port}/emulator/"
        f"v1/projects/{emulator_project}:indexUsage"  # The project here seems to be ignored 🤷‍
        f"?database=projects/{emulator_project}/databases/(default)"
    )
    request = urllib.request.Request(url)
    try:
        with urllib.request.urlopen(request) as response:
            data = response.read()
    except urllib.error.URLError as error:
        raise IndexingError(
            f"Failed to fetch indexes from emulator at {url}: {error.reason}",
        ) from error
    data = json.loads(data)
    if not data:
        # Not getting any from the emulator is sometimes expected if no queries have been run yet
        logger.info("Got empty indexes data from emulator at %s", url)
        return []
    return Index.list_from_emulator_format(data, emulator_project)


def combine_indexes(indexes1, indexes2):
    combined = set(indexes1).union(set(indexes2))
    return sorted(combined)


def write_indexes_to_definitions_file(indexes, indexes_file):
    data = {
        "indexes": [
            {
                "collectionGroup": index.collection,
                "queryScope": "COLLECTION",
                "fields": [field.as_dict() for field in index.fields],
            }
            for index in indexes
        ],
        "fieldOverrides": [],
    }
    json_data = json.dumps(data, indent=2)
    # Keep trailing newline for consistency with how editors save the file
    json_data += "\n"
    with open(indexes_file, "w") as file:
        file.write(json_data)


@dataclass
@total_ordering
class Index:
    """ Represents a Firestore index. The emulator and the definitions file provide indexes in
        different formats, so this provides a common type of object for them.
    """

    collection: str
    fields: List[IndexField]

    def __eq__(self, other):
        return self.collection == other.collection and self.fields == other.fields

    def __gt__(self, other):
        """ This is used to sort the indexes into the order in which they should be written into the
            definitions files (firestore.indexes.json).
        """
        if self.collection != other.collection:
            return self.collection > other.collection
        self_fields = self.fields[:]
        other_fields = other.fields[:]
        # Pad the two field lists with None to make them the same length. This prevents zip() from
        # masking the fact that one is longer than the other.
        while len(self_fields) < len(other_fields):
            self_fields.append(None)
        while len(other_fields) < len(self_fields):
            other_fields.append(None)

        for self_field, other_field in zip(self_fields, other_fields):
            if self_field is None:
                return True
            if other_field is None:
                return False
            if self_field != other_field:
                return self_field > other_field

    def __hash__(self):
        return hash(
            (self.collection,)
            + tuple([(index.field, index.order) for index in sorted(self.fields)])
        )

    @classmethod
    def list_from_emulator_format(cls, data, emulator_project=None) -> List[Index]:
        """ Generate indexes from the data format supplied by the Firestore emulator. """
        check_keys_equal(data, {"reports"})
        indexes = [
            cls.from_emulator_format(index_dict["index"], emulator_project=emulator_project)
            for index_dict in data["reports"]
        ]
        # Some can be None if we're filtering by project
        return filter(None, indexes)

    @classmethod
    def list_from_definitions_file_format(cls, data) -> List[Index]:
        """ Generate indexes from the data format supplied by the firestore.indexes.json file. """
        check_keys_equal(data, {"indexes", "fieldOverrides"})
        if data["fieldOverrides"]:
            raise NotImplementedError("Handling of fieldOverrides is not implemented.")
        return [cls.from_definitions_file_format(index_dict) for index_dict in data["indexes"]]

    @classmethod
    def from_emulator_format(cls, index_dict, emulator_project=None):
        check_keys_equal(index_dict, {"name", "queryScope", "fields"})
        check_item_equal(index_dict, "queryScope", "COLLECTION")

        # We have to extract the collection name from the "name" which is in this format:
        # "projects/YOUR_PROJECT/databases/(default)/collectionGroups/YOUR_DB_TABLE/indexes/_",
        parts = index_dict["name"].strip("/").split("/")
        project = parts[1]
        collection = parts[5]
        if emulator_project and project != emulator_project:
            return None

        fields = []
        for idx, field in enumerate(index_dict["fields"]):
            if "order" in field:
                check_keys_equal(field, {"fieldPath", "order"})
                check_item_in(field, "order", ("ASCENDING", "DESCENDING"))
            else:
                check_keys_equal(field, {"fieldPath", "arrayConfig"})

            # The emulator includes the automatic "__name__" field on the end of each index, but the
            # definitions file doesn't include these and Firestore complains if it does 🙄
            if field["fieldPath"] == "__name__" and idx + 1 == len(index_dict["fields"]):
                break

            fields.append(IndexField(
                field=field["fieldPath"],
                order=field.get("order"),
                array_config=field.get("arrayConfig"),
            ))
        return Index(
            collection=collection,
            fields=fields
        )

    @classmethod
    def from_definitions_file_format(cls, index_dict):
        check_keys_equal(index_dict, {"collectionGroup", "queryScope", "fields"})
        check_item_equal(index_dict, "queryScope", "COLLECTION")
        fields = []
        for field in index_dict["fields"]:
            if "order" in field:
                check_keys_equal(field, {"fieldPath", "order"})
                check_item_in(field, "order", ("ASCENDING", "DESCENDING"))
            else:
                check_keys_equal(field, {"fieldPath", "arrayConfig"})
            fields.append(IndexField(
                field=field["fieldPath"],
                order=field.get("order"),
                array_config=field.get("arrayConfig"),
            ))
        return Index(
            collection=index_dict["collectionGroup"],
            fields=fields
        )


@dataclass
@total_ordering
class IndexField:
    """ Represents a single field within a single index. """

    field: str
    # Should *either* have `order` or `array_config`
    order: Optional[Literal["ASCENDING", "DESCENDING"]]
    array_config: Optional[Literal["CONTAINS"]]  # There might be other possibilities too?

    def __eq__(self, other):
        return self.field == other.field and self.order == other.order and self.array_config == other.array_config

    def __gt__(self, other):
        """ This is used to sort the indexes into the order in which they should be written into the
            definitions files (firestore.indexes.json).
        """
        if self.field > other.field:
            return True
        elif self.field < other.field:
            return False
        # Else, if they're for the same field...
        if self.order != other.order:
            return self.order > other.order
        elif self.array_config != other.array_config:
            return self.array_config > other.array_config
        assert self == other

    def as_dict(self):
        data = {"fieldPath": self.field}
        if self.order:
            data["order"] = self.order
        else:
            data["arrayConfig"] = self.array_config
        return data


def check_keys_equal(data, keys):
    if set(data.keys()) != keys:
        raise IndexingError(f"Expected data to have keys {keys}. Data was: {data}")


def check_item_equal(data, key, value):
    if data[key] != value:
        raise IndexingError(f"Expected item '{key}' to contain '{value}'. Data was: {data}")


def check_item_in(data, key, values):
    if data[key] not in values:
        raise IndexingError(f"Expected item '{key}' to be one of {values}. Data was: {data}")
