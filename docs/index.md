Django GCloud Connectors (gcloudc)
==================================


This project provides a Django database connector / backend for the non-relational databases provided by Google Cloud Platform.

Currently it contains connectors for:

* Firestore in Datastore mode
* Firestore in native mode

It could in theory be expanded to create connectors for other non-relational databases.

### Notes

Note: from version `1.0.0` we dropped support the `OPTIMISTIC_WITH_ENTITY_GROUPS` legacy concurrency mode and legacy Datastore.
Use `0.4.0` if you need to support legacy behaviour (please be aware transactions and uniqueness constraints are buggy in  versions < `1.0.0`, for more information please refer to the [changelog](https://gitlab.com/potato-oss/google-cloud/django-gcloud-connectors/-/blob/master/CHANGELOG.md)).

This is the continuation of the Datastore connector from the [Djangae project](https://github.com/potatolondon/djangae)
but converted to use the [Cloud Datastore API](https://googleapis.github.io/google-cloud-python/latest/datastore/) on Python 3.
It is deliberately split out into its own library so that if you want to you can use Django's ORM with the Cloud Datastore on any platform.
For example, you could run Django on AWS and still use this library to connect the ORM to your Google Cloud Datastore.
