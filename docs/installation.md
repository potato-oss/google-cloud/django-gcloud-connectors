Installation
============

The most common way to install `gcloudc` is to use it as part of [Djangae](https://potato-oss.gitlab.io/djangae/djangae/).

You can also install it on its own with `pip install django-gcloud-connectors`.

However you install it, you will need to set your `DATABASES` configuration in your Django settings module to look something like this:

```python
DATABASES = {
    "default": {
        'ENGINE': 'gcloudc.db.backends.datastore',
        'PROJECT': 'YOUR_GCP_PROJECT_NAME',  # You can use djangae.environment.project_id() here
        'INDEXES_FILE': 'PATH_TO_A_FILE_FOR_SPECICAL_INDEXES',
    }
}
```

The possible engines you can choose are:

 - `gcloudc.db.backends.firestore`
 - `gcloudc.db.backends.datastore`

INDEXES_FILE is an absolute path to a yaml file that will be automatically generated. See [special_indexes.md] for more information.

## Enabling aggregation query emulation

Datastore emulator does not support aggregation quires, so when using `gcloudc.db.backends.datastore` engine,
it requires `count_mode` option to be set to "emulated" to work correctly.

```py
# settings.py
from djangae.environment import is_development_environment

DATABASES = {
    "default": {
        "ENGINE": 'gcloudc.db.backends.datastore',
        # ... other settings
        "OPTIONS": {
            "count_mode": "emulated" if is_development_environment() else "native"
        },
    }
}
```

You can also use "emulated" mode with non-emulated datastore,
since native implementation has some [limitations](https://cloud.google.com/datastore/docs/aggregation-queries#behavior_and_limitations).

_Note_: `gcloudc.db.backends.firestore` doesn't require "count_mode" options, since emulator supports aggregation queries.
=======

## Multiple database support

By default `gcloudc` will use the `default` database from your GCP project. If you want to use multiple databases, you can do so by adding additional database configurations to your `DATABASES` setting. For example:

```python
DATABASES = {
    "default": {
	'ENGINE': 'gcloudc.db.backends.datastore',
	'PROJECT': 'YOUR_GCP_PROJECT_NAME',
	'INDEXES_FILE': 'PATH_TO_A_FILE_FOR_SPECICAL_INDEXES',
	'DATABASE_ID': '(default)',
    },
    "other": {
	'ENGINE': 'gcloudc.db.backends.datastore',
	'PROJECT': 'YOUR_GCP_PROJECT_NAME',
	'INDEXES_FILE': 'PATH_TO_A_FILE_FOR_SPECICAL_INDEXES',
	'DATABASE_ID': 'my-other-database',
    }
}

Note that to refer to the default database explicitly you must use the string `(default)` inclusing the parenthesis or just omit the option. See [GCP documentation](https://cloud.google.com/datastore/docs/manage-databases) for more information on how to create multiple databases in a single project.

```

## Automatic Cloud Datastore Emulator startup

gcloudc provides overrides for the `runserver` and `test` commands which
start and stop a Cloud Datastore Emulator instance. To enable this functionality add `gcloudc.commands` _at the beginning_ of your `INSTALLED_APPS` setting.
