# Special Indexes

gcloudc comes with a built-in mechanism to pre-calculate some entity properties automatically to
allow more complex queries than would normally be possible on a non-relational database.

For example, take the following Django queryset filter:

```
    User.objects.filter(first_name__icontains="bob")
```

This sort of case-insensitive contains query would not normally be possible, however, with a pre-generated document property it can be. In this situation, gcloudc will automatically create an additional descendent document when a User is saved, and on that document it will calculate lower-cased permutations of `first_name` and store them in a list property.

The above filter will be transformed into a query on that list property so that the correct documents can
be returned.

For this functionality to work, gcloudc needs a record of which fields, on which models, need which
pre-calculated fields. This information is stored in a yaml file, and the path to that yaml file
can be specified using the INDEXES_FILE property on settings.DATABASES.

For more explicit querying, you may also wish to explore the [computed fields](fields.md#computed-fields).

## Caveats

Using special indexes heavily will increase the storage use of your database - these kinds of queries
might also suffer from reduced performance or have limitations when paginating results.