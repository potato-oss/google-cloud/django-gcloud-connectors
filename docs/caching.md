Caching
============

gcloudc has a minimal caching layer inside transactions that works around the surprising behaviour of
Datastore / Firestore transaction isolation. With these databases one of two things happens if you read
an object after writing it inside a transaction:

 - Datastore: The object is returned in the state it was at the start of the transaction (ignoring your write)
 - Firestore: An error will be thrown

To make things a little more like normal Django backends, each time you write an object inside a transaction, it
is stored in a local cache. From that point on while inside the transaction, the following behaviour will occur:

 - If you fetch the object by its primary key, it will be returned from the cache
 - If you query the object by a unique field or unique combination, it will be returned from the cache
 - If you perform a query that would've matched a previously written object, an error is thrown

Queries that do not match any objects previously written inside the transaction will operate normally.

